import QtQuick 2.0
import Sailfish.Silica 1.0
import Nemo.Configuration 1.0
import Nemo.DBus 2.0
import org.nemomobile.systemsettings 1.0

Page {
    id: page

    ConfigurationGroup {
        id: conf
        path: "/org/coderus/screencast"
        property int buffers: 60
        property real scale: 0.5
        property int quality: 90
        property bool smooth: true
        property string username
        property string password
        property var clients
        property bool flush: false
    }

    property bool socketEnabled: false
    property bool updateInProgress: false
    Component.onCompleted: { updateSocketState() }
    onStatusChanged: { if (status != PageStatus.Active) { timer.stop() } else { timer.start() } }

    function setSocketState(b) {
        socketEnabled = b;
        console.debug("Toggled: " + b);
    }
    function updateSocketState() {
        updateInProgress: true
        systemd.checkUnitList();
        updateInProgress: false
    }
    Timer {
        id: timer
        triggeredOnStart: true
        repeat: true
        interval: 5000
        onTriggered: updateSocketState()
    }
    // systemd manager
    DBusInterface {
        id: systemd
        bus: DBus.SessionBus
        service: "org.freedesktop.systemd1"
        path: "/org/freedesktop/systemd1"
        iface: "org.freedesktop.systemd1.Manager"
        function checkUnitList() {
          /*
          ~~~ FIXME ~~~
          we should really be using one of these, but they always come back empty.
          so we workaround by 'select * from table | grep foo'

          //typedCall('ListUnitsFiltered', [
          //typedCall('ListUnitsByPatterns', [
          //typedCall('ListUnitsByNames', [
                            //{ 'type': 'as', 'value': [ { 'type': 's', 'value': 'listening' } ] } //,
                            //{ 'type': 'as', 'value': [ { 'type': 's', 'value': 'screencast*' } ] }
                      ],
          ~~~~~~~~~~~~~

          also this should really not set/toggle state, do that in the calling function.

          */
          typedCall('ListUnits', [ ],
                      function (result) {   //console.debug('DBus call result: ' + result) ;
                                            if (!result) { console.debug("DBus call returned empty result list!"); } else {
                                              //console.debug("No of results: " + result.length );
                                              for ( var index in result.reverse() ) {
                                                  if ( result[index].toString().match(/screencast.socket/) && result[index].toString().match(/listening/) ) {
                                                      //console.debug("found at index " + index + "!");
                                                      setSocketState(true);
                                                      return true;
                                                  }
                                              }
                                          }
                                          setSocketState(false);
                      },
                      function (error, message) { console.warn('DBus call failed: ', error, 'message: ', message) }
              )
        }
        function stopSocket() {
          call('StopUnit', [ 'screencast.socket', 'replace' ],
                      function (result) {         console.debug('DBus call result: ' + result) },
                      function (error, message) { console.warn('DBus call failed: ', error, 'message: ', message) }
               )
          setSocketState(false);
        }
        function startSocket() {
          call('StartUnit', [ 'screencast.socket', 'replace' ],
                      function (result) {         console.debug('DBus call result: ' + result) },
                      function (error, message) { console.warn('DBus call failed: ', error, 'message: ', message) }
               )
          setSocketState(true);
        }
    }

    DeveloperModeSettings {
        id: developerModeSettings
    }

    SilicaFlickable {
        id: flick
        anchors.fill: parent
        contentHeight: column.height

        Column {
            id: column
            width: parent.width

            PageHeader {
                title: qsTr("Screencast")
            }

            SectionHeader {
                text: qsTr("Service Status")
            }

            TextSwitch {
                checked: page.socketEnabled
                automaticCheck : false
                text: checked ? qsTr("Waiting for connections.") : qsTr("Inactive.")
                //description: checked ? qsTr("Tap to disable.") : qsTr("Tap to enable.")
                onClicked: {
                    busy = true;
                    checked ? systemd.stopSocket() : systemd.startSocket();
                    busy = false;
                }
            }

            SectionHeader {
                text: qsTr("Main options")
            }

            Label {
                width: parent.width - Theme.horizontalPageMargin * 2
                anchors.horizontalCenter: parent.horizontalCenter
                textFormat: Text.PlainText
                font.pixelSize: Theme.fontSizeSmall
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                text: qsTr("After modifying options below you need to disconnect all clients and connect again for them to take effect.")
            }

            Slider {
                id: buffersSlider
                width: parent.width
                minimumValue: 2
                maximumValue: 60
                stepSize: 1
                value: conf.buffers
                label: qsTr("Buffers to store frames")
                valueText: qsTr("%1").arg(value)
                onReleased: {
                    conf.buffers = value
                }
            }

            Slider {
                id: qualitySlider
                width: parent.width
                minimumValue: 10
                maximumValue: 100
                stepSize: 1
                value: conf.quality
                label: qsTr("JPEG frames quality")
                valueText: qsTr("%1%").arg(value)
                onReleased: {
                    conf.quality = value
                }
            }

            Slider {
                id: scaleSlider
                width: parent.width
                minimumValue: 1
                maximumValue: 100
                stepSize: 1
                value: conf.scale * 100
                label: qsTr("Frames scale")
                valueText: qsTr("%1%").arg(value)
                onReleased: {
                    conf.scale = value / 100
                }
            }

            TextSwitch {
                id: smoothSwitch
                visible: conf.scale < 1.0
                text: qsTr("Apply smooth transformation")
                checked: conf.smooth
                automaticCheck: false
                onClicked: {
                    conf.smooth = !checked
                }
            }

            TextSwitch {
                id: flushSwitch
                text: qsTr("Flush packages")
                checked: conf.flush
                automaticCheck: false
                onClicked: {
                    conf.flush = !checked
                }
            }

            SectionHeader {
               text: qsTr("Authorization")
            }

            TextField {
                id: usernameField
                width: parent.width
                label: qsTr("Username")
                placeholderText: qsTr("Username")
                inputMethodHints: Qt.ImhNoAutoUppercase
                text: conf.username
                EnterKey.iconSource: "image://theme/icon-m-enter-next"
                EnterKey.onClicked: {
                    conf.username = text
                    passwordField.focus = true
                }
            }

            PasswordField {
                id: passwordField
                width: parent.width
                label: qsTr("Password")
                placeholderText: qsTr("Password")
                text: conf.password
                EnterKey.iconSource: "image://theme/icon-m-enter-accept"
                EnterKey.onClicked: {
                    conf.password = text
                    focus = false
                }
            }

            SectionHeader {
                text: qsTr("You can connect to")
            }

            LinkedLabel {
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.margins: Theme.horizontalPageMargin
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                property string authParams: (conf.username.length > 0 && conf.password.length > 0) ? "%1:%2@".arg(usernameField.text).arg(passwordField.text) : ""
                plainText: (developerModeSettings.wlanIpAddress ? qsTr("\nhttp://%1%2:5554").arg(authParams).arg(developerModeSettings.wlanIpAddress) : "")
                    + (developerModeSettings.usbIpAddress ? qsTr("\nhttp://%1%2:5554").arg(authParams).arg(developerModeSettings.usbIpAddress) : "")
                defaultLinkActions: false
                onLinkActivated: Clipboard.text = url
            }

            SectionHeader {
                text: qsTr("%n connected clients", "0", clientsRepeater.count)
                visible: clientsRepeater.count > 0
            }

            Repeater {
                id: clientsRepeater
                model: conf.clients
                delegate: Label {
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.margins: Theme.horizontalPageMargin
                    text: modelData
                }
            }

            Item {
                height: Theme.paddingLarge
                width: 1
            }

            Label {
                width: parent.width - Theme.horizontalPageMargin * 2
                anchors.horizontalCenter: parent.horizontalCenter
                textFormat: Text.PlainText
                font.pixelSize: Theme.fontSizeSmall
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                text: qsTr("After connecting you have to do something on the phone so new frames are sent to clients.")
            }

            Item {
                height: Theme.paddingLarge
                width: 1
            }
        }

        VerticalScrollDecorator {}
    }
}
